package com.example.pamapplication.activities

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.pamapplication.R
import com.example.pamapplication.utils.RealPathUtil
import kotlin.system.exitProcess

class MainActivity : AppCompatActivity() {

    private var buttonChooseFile: Button? = null;
    private var buttonRecordNew: Button? = null;
    private var buttonExitApp: Button? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonChooseFile = findViewById(R.id.buttonChooseFile)
        buttonChooseFile?.setOnClickListener{
            val chooseFile = Intent(Intent.ACTION_GET_CONTENT)
            chooseFile.addCategory(Intent.CATEGORY_OPENABLE)
            chooseFile.type = "audio/mpeg"
            startActivityForResult(
                    Intent.createChooser(chooseFile, "Choose a file"),
                    111
            )
        }

        buttonRecordNew = findViewById(R.id.buttonRecordNew)
        buttonRecordNew?.setOnClickListener{
            goToRecordingActivity()
        }

        buttonExitApp = findViewById(R.id.buttonExitApp)
        buttonExitApp?.setOnClickListener{
            exitProcess(0)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 111 && resultCode == RESULT_OK) {
            val selectedFile = data?.data.getFilePath(this) //The uri with the location of the file
            goToSettingsActivity(selectedFile)
        }
    }

    private fun goToRecordingActivity() {
        val intent = Intent(this, RecordingActivity::class.java)
        startActivity(intent)
    }

    private fun goToSettingsActivity(recordingPath: String) {
        val intent = Intent(this, SettingsActivity::class.java)
        intent.putExtra("recordingPath", recordingPath)
        startActivity(intent)
    }

    private fun Uri?.getFilePath(context: Context): String {
        return this?.let { uri -> RealPathUtil.getRealPath(context, uri) ?: "" } ?: ""
    }
}