package com.example.pamapplication.activities

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaRecorder
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.pamapplication.R
import java.io.IOException

class RecordingActivity : AppCompatActivity() {

    private var mediaRecorder: MediaRecorder? = null
    private var isRecording: Boolean = false
    private var recordingStopped: Boolean = false

    private var buttonStartRecording: Button? = null;
    private var buttonPauseRecording: Button? = null;
    private var buttonStopRecording: Button? = null;
    private var buttonBackToMainActivity: Button? = null;

    private var output: String? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recording)
        output = getExternalFilesDir(null)?.absolutePath + "/recording.mp3"

        buttonStartRecording = findViewById(R.id.buttonStartRecording)
        buttonStartRecording?.setOnClickListener{
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                val permissions = arrayOf(android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                ActivityCompat.requestPermissions(this, permissions,0)
            } else {
                startRecording()
            }
        }

        buttonPauseRecording = findViewById(R.id.buttonPauseRecording)
        buttonPauseRecording?.setOnClickListener {
                pauseRecording()
            }

        buttonStopRecording = findViewById(R.id.buttonStopRecording)
        buttonStopRecording?.setOnClickListener{
           stopRecording()
        }

        buttonBackToMainActivity = findViewById(R.id.buttonBackToMainActivity1)
        buttonBackToMainActivity?.setOnClickListener{
            finish()
        }
    }

    private fun startRecording() {
        initializeMediaRecorder()
        try {
            mediaRecorder?.prepare()
            mediaRecorder?.start()
            isRecording = true
            Toast.makeText(this, "Nagrywanie rozpoczęte", Toast.LENGTH_SHORT).show()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    @SuppressLint("RestrictedApi", "SetTextI18n")
    @TargetApi(Build.VERSION_CODES.N)
    private fun pauseRecording() {
        if(isRecording) {
            if(!recordingStopped){
                Toast.makeText(this,"Nagrywanie zatrzymane", Toast.LENGTH_SHORT).show()
                mediaRecorder?.pause()
                recordingStopped = true
                buttonPauseRecording?.text = "Wznów"
            }else{
                resumeRecording()
            }
        }
    }

    @SuppressLint("RestrictedApi", "SetTextI18n")
    @TargetApi(Build.VERSION_CODES.N)
    private fun resumeRecording() {
        Toast.makeText(this,"Wznawiam", Toast.LENGTH_SHORT).show()
        mediaRecorder?.resume()
        buttonPauseRecording?.text = "Pauza"
        recordingStopped = false
    }

    private fun stopRecording(){
        if(isRecording){
            mediaRecorder?.stop()
            mediaRecorder?.release()
            isRecording = false
            Toast.makeText(this, "Nagranie zostało zapisane", Toast.LENGTH_SHORT).show()
            goToSettingsActivity()
        }else{
            Toast.makeText(this, "Niczego nie nagrywasz w tym momencie", Toast.LENGTH_SHORT).show()
        }
    }
    
    private fun goToSettingsActivity() {
        val intent = Intent(this, SettingsActivity::class.java)
        intent.putExtra("recordingPath", output)
        startActivity(intent)
    }

    private fun initializeMediaRecorder() {
        mediaRecorder = MediaRecorder()
        mediaRecorder?.setAudioSource(MediaRecorder.AudioSource.MIC)
        mediaRecorder?.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
        mediaRecorder?.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
        mediaRecorder?.setAudioSamplingRate(48000)
        mediaRecorder?.setAudioChannels(1)
        mediaRecorder?.setOutputFile(output)
    }
}
