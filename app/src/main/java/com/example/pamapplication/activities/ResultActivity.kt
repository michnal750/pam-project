package com.example.pamapplication.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.example.pamapplication.R

class ResultActivity : AppCompatActivity() {

    private var FinalTonalityResultTextView: TextView? = null
    private var KKrumhanslResultTextView: TextView? = null
    private var TemperResultTextView: TextView? = null
    private var AShanaResultTextView: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        FinalTonalityResultTextView = findViewById(R.id.FinalTonalityTextView)
        FinalTonalityResultTextView?.text = intent.getStringExtra("FinalTonality")

        KKrumhanslResultTextView = findViewById(R.id.KKrumhanslTextView)
        KKrumhanslResultTextView?.text = intent.getStringExtra("KKrumhansl")

        TemperResultTextView = findViewById(R.id.TemperTextView)
        TemperResultTextView?.text = intent.getStringExtra("Temper")

        AShanaResultTextView = findViewById(R.id.AShanaTextView)
        AShanaResultTextView?.text = intent.getStringExtra("AShana")

        var buttonBackToPreviousActivity: Button = findViewById(R.id.buttonBackToPreviousActivity3)
        buttonBackToPreviousActivity.setOnClickListener{
            finish()
        }

        var buttonBackToMainActivity: Button = findViewById(R.id.buttonBackToMainActivity2)
        buttonBackToMainActivity.setOnClickListener{
            finishAffinity()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}