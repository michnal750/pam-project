package com.example.pamapplication.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.InputFilter
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import cafe.adriel.androidaudioconverter.AndroidAudioConverter
import cafe.adriel.androidaudioconverter.callback.IConvertCallback
import cafe.adriel.androidaudioconverter.callback.ILoadCallback
import cafe.adriel.androidaudioconverter.model.AudioFormat
import com.example.pamapplication.R
import com.example.pamapplication.keyFinding.NoteCalculation
import com.example.pamapplication.keyFinding.NoteName
import com.example.pamapplication.keyFinding.ToneAnalyser
import com.example.pamapplication.keyFinding.Vector
import com.example.pamapplication.frequency.FrequencyCalculations
import com.example.pamapplication.utils.InputFilterMinMax
import java.io.File
import java.nio.ByteBuffer
import kotlin.math.abs

class SettingsActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private var buttonBackPreviousActivity: Button? = null
    private var calculateModeButton: Button? = null

    private var numberOfSamplesList = arrayOf("128", "256", "512", "1024", "2048", "4096", "8192")
    private var spinner:Spinner? = null
    private var numberOfSamples: Int = 128

    private var checkBox: CheckBox? = null
    private var maxNumberOfNotesEditText: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        AndroidAudioConverter.load(this, object : ILoadCallback {
            override fun onSuccess() {
            }

            override fun onFailure(error: java.lang.Exception) {
            }
        })

        val recordingPath = intent.getStringExtra("recordingPath")

        checkBox = findViewById(R.id.checkBox)
        var noteNumberTextView: TextView = findViewById(R.id.noteNumberTextView)

        maxNumberOfNotesEditText = findViewById(R.id.noteNumberEditTextNumber)
        maxNumberOfNotesEditText?.filters =  arrayOf<InputFilter>(InputFilterMinMax(1, 10000))
        checkBox?.setOnCheckedChangeListener { _, isChecked ->
            noteNumberTextView.isEnabled = !isChecked
            maxNumberOfNotesEditText?.isEnabled = !isChecked
        }

        buttonBackPreviousActivity = findViewById(R.id.buttonBackToPreviousActivity2)
        buttonBackPreviousActivity?.setOnClickListener{
            finish()
        }

        calculateModeButton = findViewById(R.id.calculateModeButton)
        calculateModeButton?.setOnClickListener{

            val audioFile = File(recordingPath)
            val callback: IConvertCallback = object : IConvertCallback {
                override fun onSuccess(convertedFile: File) {
                    calculateFrequencies(convertedFile)
                }
                override fun onFailure(error: java.lang.Exception) {
                }
            }
            AndroidAudioConverter.with(this)
                    .setFile(audioFile)
                    .setFormat(AudioFormat.WAV)
                    .setCallback(callback)
                    .convert()
        }

        spinner = findViewById(R.id.spinner)
        spinner!!.onItemSelectedListener = this
        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, numberOfSamplesList)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner!!.adapter = arrayAdapter
    }

    private fun convertFileToByteArray(outputFile: File): ByteArray {
        //It will save the file in phone's internal memory directory.
        var bytearray = ByteArray(0)
        try
        {
            val inputStream = contentResolver.openInputStream(Uri.fromFile(outputFile))
            bytearray = inputStream!!.readBytes()
        }
        catch (e: Exception) {
            e.printStackTrace()
        }
        return bytearray
    }

    private fun calculateFrequencies(convertedFile: File){
        var byteArray = convertFileToByteArray(convertedFile)

        // częstotliwość próbkowania
        val sampleRateByteArray: ByteArray = byteArray.copyOfRange(24, 28).reversedArray()
        val sampleRate: Int = ByteBuffer.wrap(sampleRateByteArray).int

        // format, jeżeli jest równy 1 to PCM
        val formatTypeByteArray: ByteArray = byteArray.copyOfRange(20, 22).reversedArray()
        val formatType: Short = ByteBuffer.wrap(formatTypeByteArray).short

        // liczba kanałów
        val channelNumberTypeByteArray: ByteArray = byteArray.copyOfRange(22, 24).reversedArray()
        val channelNumber: Short = ByteBuffer.wrap(channelNumberTypeByteArray).short

        // rozdzielczość, jeżeli 8 to wartości są od -128 do 128
        val bitsPerSampleByteArray: ByteArray = byteArray.copyOfRange(34, 36).reversedArray()
        val bitsPerSample: Short = ByteBuffer.wrap(bitsPerSampleByteArray).short

        // usunięcie nagłówka
        byteArray = byteArray.copyOfRange(44, byteArray.size)

        val frequencyCalculations = FrequencyCalculations(sampleRate)
        val numberOfBytes = numberOfSamples * 2
        if (byteArray.size < numberOfBytes ){
            Toast.makeText(this, "Zbyt krótki plik", Toast.LENGTH_SHORT).show()
            return
        }

        val frequenciesArray : MutableList<Double> = mutableListOf<Double>()
        for(i in 0..byteArray.size step numberOfBytes )
        {
            if ( i + numberOfBytes >= byteArray.size ){
                continue
            }
            var newByteArray: ShortArray = prepareData(byteArray.copyOfRange(i, i + numberOfBytes))
            frequenciesArray.add(abs(frequencyCalculations.getFrequency(newByteArray)))
        }

        /* mock służący do testowania
            val frequenciesArray2: MutableList<Double> = mutableListOf<Double>()
        frequenciesArray2.add(987.77) //B
        frequenciesArray2.add(987.77)
        frequenciesArray2.add(987.77)
        frequenciesArray2.add(987.77)
        frequenciesArray2.add(987.77)
        frequenciesArray2.add(987.77)
        frequenciesArray2.add(987.77)
        frequenciesArray2.add(987.77)
        frequenciesArray2.add(1174.66) //D
        frequenciesArray2.add(1174.66)
        frequenciesArray2.add(1479.98) //F#
        frequenciesArray2.add(1479.98)
        frequenciesArray2.add(1479.98)
        frequenciesArray2.add(1760.0) //A
        frequenciesArray2.add(1760.0)
        frequenciesArray2.add(1760.0)
        frequenciesArray2.add(1760.0)
        frequenciesArray2.add(1760.0)
        frequenciesArray2.add(1760.0)
        frequenciesArray2.add(1760.0)
        frequenciesArray2.add(1567.98) //G
        frequenciesArray2.add(1567.98)
         */
        var maxNumberOfNotes: Int? = null
                if (!checkBox?.isChecked!!){
                    maxNumberOfNotes = maxNumberOfNotesEditText?.text.toString().toInt()
                }
            var notesVector: NoteCalculation = NoteCalculation(frequenciesArray)
            var notesNames: ArrayList<NoteName> = notesVector.getNoteNames()
            var normalizedVector: Vector = Vector(notesNames, maxNumberOfNotes)
            var normalized = normalizedVector.getNormalizedVector()
            var toneAnalyser: ToneAnalyser = ToneAnalyser(normalized)
            var KKrumhansl = toneAnalyser.findKruhanslTonality() ?: "-"
            var Temper = toneAnalyser.findTemperleyTonality() ?: "-"
            var AShana = toneAnalyser.findAlbrechtTonality() ?: "-"
            var FinalTonality = toneAnalyser.findTonality() ?: "-"

        goToResultActivity(KKrumhansl, Temper, AShana, FinalTonality)
    }

    private fun prepareData(byteArray: ByteArray) : ShortArray{
        var newShortArray: ShortArray = ShortArray(byteArray.size / 2)
        var iterator = 0
        for(i in 0 .. byteArray.size - 1 step 2 ) {
        val a: ByteArray = byteArray.copyOfRange(i, i + 2).reversedArray()
            var shortNumber: Short = ByteBuffer.wrap(a).short
        newShortArray[iterator] = shortNumber
                iterator++
        }
        return newShortArray
    }

    private fun goToResultActivity(KKrumhansl: String, Temper: String, AShana: String, FinalTonality: String) {
        val intent = Intent(this, ResultActivity::class.java)
        intent.putExtra("KKrumhansl", KKrumhansl)
        intent.putExtra("Temper", Temper)
        intent.putExtra("AShana", AShana)
        intent.putExtra("FinalTonality", FinalTonality)
        startActivity(intent)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        numberOfSamples = spinner?.selectedItem.toString().toInt()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }
}