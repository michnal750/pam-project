package com.example.pamapplication.frequency;

public class FrequencyCalculations {
    private int sampleRate;

    public FrequencyCalculations(int sampleRate){
        this.sampleRate = sampleRate;
    }
    private FFT fft = new FFT();
    /**
     * Calculates the Frequency based off of the byte array,
     * @param shortData The audioData you want to analyze
     * @return The calculated frequency in Hertz.
     */
    public double getFrequency(short[] shortData){
        double[] audioData = this.toDoubleArray(shortData);
        audioData = applyHanningWindow(audioData);
        Complex[] complex = new Complex[audioData.length];
        for(int i = 0; i<complex.length; i++){
            complex[i] = new Complex(audioData[i], 0);
        }
        Complex[] fftTransformed = fft.fft(complex);
        return calculateFundamentalFrequency(fftTransformed,4);
    }

    private double[] applyHanningWindow(double[] data){
        return applyHanningWindow(data, 0, data.length);
    }

    private double[] applyHanningWindow(double[] signal_in, int pos, int size)
    {
        for (int i = pos; i < pos + size; i++)
        {
            int j = i - pos; // j = index into Hann window function
            signal_in[i] = (double)(signal_in[i] * 0.5 * (1.0 - Math.cos(2.0 * Math.PI * j / size)));
        }
        return signal_in;
    }

    /**
     * Harmonic Product Spectrum
     * @param fftData
     * @param n
     * @return
     */
    private double calculateFundamentalFrequency(Complex[] fftData, int n){

        Complex[][] data = new Complex[n][fftData.length/n];
        for(int i = 0; i<n; i++){
            for(int j = 0; j<data[0].length; j++){
                data[i][j] = fftData[j*(i+1)];
            }
        }
        Complex[] result = new Complex[fftData.length/n];//Combines the arrays
        for(int i = 0; i<result.length; i++){
            Complex tmp = new Complex(1,0);
            for(int j = 0; j<n; j++){
                tmp = tmp.times(data[j][i]);
            }
            result[i] = tmp;
        }

        //Calculates Maximum Magnitude of the array
        double max = Double.MIN_VALUE;
        int index = -1;
        for(int i = 0; i<result.length; i++){
            Complex c = result[i];
            double tmp = c.abs();
            if(tmp>max){
                max = tmp;;
                index = i;
            }
        }
        return (double)(index * sampleRate) / fftData.length;
    }

    private double[] toDoubleArray(short[] shortArray){
        double[] doubles = new double[shortArray.length];
        for(int i=0; i<doubles.length; i++){
            doubles[i] = (double)shortArray[i];
        }
        return doubles;
    }
}
