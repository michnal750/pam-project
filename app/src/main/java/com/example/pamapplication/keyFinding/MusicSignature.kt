package com.example.pamapplication.keyFinding

class MusicSignature {
    private val numberOfNotes = 12
    private val NOTE_NAMES = arrayOf(
            NoteName.Dis,
            NoteName.Gis,
            NoteName.Cis,
            NoteName.Fis,
            NoteName.B,
            NoteName.E,
            NoteName.A,
            NoteName.D,
            NoteName.G,
            NoteName.C,
            NoteName.F,
            NoteName.Ais
    )
    private var circleOfFifth: DoubleArray
    private var characteristicAxis: DoubleArray

    constructor(vector: DoubleArray) {
        circleOfFifth = DoubleArray(numberOfNotes)
        characteristicAxis = DoubleArray(numberOfNotes);
        circleOfFifth[0] = vector[NoteName.A.ordinal]
        circleOfFifth[1] = vector[NoteName.D.ordinal]
        circleOfFifth[2] = vector[NoteName.G.ordinal]
        circleOfFifth[3] = vector[NoteName.C.ordinal]
        circleOfFifth[4] = vector[NoteName.F.ordinal]
        circleOfFifth[5] = vector[NoteName.Ais.ordinal]
        circleOfFifth[6] = vector[NoteName.Dis.ordinal]
        circleOfFifth[7] = vector[NoteName.Gis.ordinal]
        circleOfFifth[8] = vector[NoteName.Cis.ordinal]
        circleOfFifth[9] = vector[NoteName.Fis.ordinal]
        circleOfFifth[10] = vector[NoteName.B.ordinal]
        circleOfFifth[11] = vector[NoteName.E.ordinal]
        countAxisValues()
    }

    private fun countAxisValues() {
        for (i in 0 until numberOfNotes) {
            characteristicAxis[i] = countAxisValue(circleOfFifth, i)
        }
    }

    private fun countAxisValue(circle: DoubleArray, shift: Int): Double {
        var result = 0.0
        val maxArrayBound: Int = numberOfNotes - 1
        val startOfRange = 1
        val endOfRange = 6
        for (indexofNumberToCount in startOfRange + shift until endOfRange + shift) {
            var index = indexofNumberToCount
            if (index > maxArrayBound) index -= numberOfNotes
            result += circle[index]
            if (index + endOfRange > maxArrayBound) index -= numberOfNotes
            result -= circle[index + endOfRange]
        }
        return result
    }

    fun getMostProbablyMinorKey(): NoteName? {
        var minorKey = findMaxAxisValue()
        if(minorKey == null) {
            return null
        }
        val differenceInKeys = 3
        minorKey -= differenceInKeys

        if (minorKey < 0) minorKey += numberOfNotes
        return NOTE_NAMES[minorKey]
    }

    fun getMostProbablyMajorKey(): NoteName? {
        var majorKey = findMaxAxisValue()
        if(majorKey == null) {
            return null
        }
        return NOTE_NAMES[majorKey]
    }

    private fun findMaxAxisValue(): Int? {
        var x: Int
        var index = characteristicAxis.indexOf(characteristicAxis.maxOrNull()!!)
        var maxValue = characteristicAxis.maxOrNull()
        for (i in characteristicAxis.indices) {
            if (characteristicAxis[i] == maxValue && i != index) {
                return null;
            }
        }
        x = index
        if (x == 0) {
            x = numberOfNotes - 1
        } else {
            x--
        }
        return x
    }
}


