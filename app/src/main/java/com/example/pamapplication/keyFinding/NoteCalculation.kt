package com.example.pamapplication.keyFinding

import kotlin.math.log
import kotlin.math.roundToInt

class NoteCalculation {
    private var notes: MutableList<Double> = mutableListOf<Double>()
    private var notesNames: ArrayList<NoteName> = ArrayList<NoteName>()

    constructor(notesList: MutableList<Double>) {
        this.notes = notesList;
    }

    fun getNoteNames(): ArrayList<NoteName> {
        var note: NoteName
        for (item in notes) {
            if (item > 0) {
                var midiIndex = (69.0 + 12.0 * log(item / 440.0, 2.0)).roundToInt()
                var noteIndex: Int = midiIndex % 12
                note = NoteName.values()[noteIndex]
                notesNames.add(note)
            }
        }
        return notesNames
    }
}