package com.example.pamapplication.keyFinding

import kotlin.math.pow
import kotlin.math.sqrt

class ToneAnalyser {
    private val kkProfileCdur =
        doubleArrayOf(6.35, 2.23, 3.48, 2.33, 4.38, 4.09, 2.52, 5.19, 2.39, 3.66, 2.29, 2.88)
    private val kkProfileCmoll =
        doubleArrayOf(6.33, 2.68, 3.52, 5.38, 2.60, 3.53, 2.54, 4.75, 3.98, 2.69, 3.34, 3.17)
    private val temProfileCdur =
        doubleArrayOf(5.0, 2.0, 3.5, 2.0, 4.5, 4.0, 2.0, 4.5, 2.0, 3.5, 1.5, 4.0)
    private val temProfileCmoll =
        doubleArrayOf(5.0, 2.0, 3.5, 4.5, 2.0, 4.0, 2.0, 4.5, 3.5, 2.0, 1.5, 4.0)
    private val asProfileCdur =
        doubleArrayOf(
            0.238, 0.006, 0.111, 0.006, 0.137, 0.094, 0.016, 0.214, 0.009, 0.080, 0.008, 0.081
        )
    private val asProfileCmoll = doubleArrayOf(
        0.220, 0.006, 0.104, 0.123, 0.019, 0.103, 0.012, 0.214, 0.062, 0.022, 0.061, 0.052
    )

    var normalizedVector: DoubleArray
    var tonalityNames = arrayOf("C", "D♭", "D", "E♭", "E", "F", "F♯", "G", "A♭", "A", "B♭", "B")

    constructor(normalizedVector: DoubleArray) {
        this.normalizedVector = normalizedVector
    }

    private fun findKeyUsingProfiles(profileCdur: DoubleArray, profileCmoll: DoubleArray): String? {
        var Tonality: String? = null
        var result: String? = null
        val MajorCorrels = DoubleArray(profileCdur.size)
        val MinorCorrels = DoubleArray(profileCmoll.size)
        for (i in profileCdur.indices) {
            MajorCorrels[i] = pearsonCorrellation(normalizedVector, shiftArray(profileCdur, i))
            MinorCorrels[i] = pearsonCorrellation(normalizedVector, shiftArray(profileCmoll, i)
            )
        }
        val index: Int
        if (MajorCorrels.max()!! > MinorCorrels.max()!!) {
            index = MajorCorrels.indexOf(MajorCorrels.max()!!)
            result = "Dur"
        } else {
            index = MinorCorrels.indexOf(MinorCorrels.max()!!)
            result = "moll"
        }
        for (note in NoteName.values()) {
            if (note.ordinal == index) Tonality = tonalityNames[note.ordinal]
        }
        if (result == "moll") {
            if (Tonality == "D♭") {
                Tonality = "C♯"
            }
            if (Tonality == "A♭") {
                Tonality = "G♯"
            }
            Tonality = Tonality!!.toLowerCase()
        }
        return "$Tonality-$result"
    }

    fun findKruhanslTonality(): String? {
        return findKeyUsingProfiles(kkProfileCdur, kkProfileCmoll)
    }

    fun findTemperleyTonality(): String? {
        return findKeyUsingProfiles(temProfileCdur, temProfileCmoll)
    }

    fun findAlbrechtTonality(): String? {
        return findKeyUsingProfiles(asProfileCdur, asProfileCmoll)
    }

    fun findTonality(): String? {
        val musicSignature = MusicSignature(normalizedVector!!)
        val majorKey = musicSignature.getMostProbablyMajorKey()
        val minorKey = musicSignature.getMostProbablyMinorKey()
        if (majorKey == null || minorKey == null)
            return null
        val MajorProfile: DoubleArray =
            shiftArray(kkProfileCdur, majorKey!!.ordinal)
        val MinorProfile: DoubleArray =
            shiftArray(kkProfileCmoll, minorKey!!.ordinal)
        val higherPearsonCorrelation: Double =
            pearsonCorrellation(normalizedVector!!, MajorProfile)
        var Tonality = tonalityNames[majorKey.ordinal] + "-Dur"
        if (higherPearsonCorrelation < pearsonCorrellation(
                normalizedVector!!,
                MinorProfile
            )
        ) {
            Tonality = tonalityNames[minorKey.ordinal]
            if (Tonality == "D♭") {
                Tonality = "C♯"
            }
            if (Tonality == "A♭") {
                Tonality = "G♯"
            }
            Tonality = Tonality.toLowerCase() + "-moll"
        }
        return Tonality
    }

    private fun shiftArray(array: DoubleArray, shift: Int): DoubleArray {
        val newArray = DoubleArray(array.size)
        for (index in array.indices) {
            var newIndex = index + shift
            if (newIndex > array.size - 1) {
                newIndex -= array.size
            }
            newArray[newIndex] = array[index]
        }
        return newArray
    }

    private fun pearsonCorrellation(firstData: DoubleArray, secondData: DoubleArray): Double {
        val arraysLength = firstData.size.toDouble()
        var sumOfFirstData = 0.0
        var sumOfSecondData = 0.0
        var sumOfMultipliedBothData = 0.0
        var sumOfSquaredFirstData = 0.0
        var sumOfSquaredSecondData = 0.0
        var i = 0
        while (i < arraysLength) {
            sumOfFirstData += firstData[i]
            sumOfSecondData += secondData[i]
            sumOfMultipliedBothData += firstData[i] * secondData[i]
            sumOfSquaredFirstData += firstData[i].pow(2.0)
            sumOfSquaredSecondData += secondData[i].pow(2.0)
            i++
        }
        return (arraysLength * sumOfMultipliedBothData - sumOfFirstData * sumOfSecondData) /
                sqrt(
                    (arraysLength * sumOfSquaredFirstData - sumOfFirstData.pow(2.0)) * (arraysLength * sumOfSquaredSecondData - sumOfSecondData.pow(
                        2.0
                    ))
                )
    }
}