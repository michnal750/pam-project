package com.example.pamapplication.keyFinding

enum class NoteName {
    C,
    Cis,
    D,
    Dis,
    E,
    F,
    Fis,
    G,
    Gis,
    A,
    Ais,
    B
}