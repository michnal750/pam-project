package com.example.pamapplication.keyFinding

class Vector {
    private val numberOfNotes: Int = 12
    private val notesDuration: ArrayList<ScaleNote>
    private var noteArrayList: ArrayList<NoteName>? = null
    private var maxNumberOfNotes: Int? = null

    constructor(noteArray: ArrayList<NoteName>?, maxNumber: Int?) {
        noteArrayList = noteArray
        this.notesDuration = ArrayList<ScaleNote>(numberOfNotes)
        for (i in 0 until numberOfNotes) {
            notesDuration.add(ScaleNote())
            notesDuration.last().noteName = NoteName.values()[i]
        }
        maxNumberOfNotes = maxNumber;
    }

    fun getNormalizedVector(): DoubleArray {
        if (maxNumberOfNotes != null && noteArrayList?.size!! > maxNumberOfNotes!!) {
            var noteArrayList2: ArrayList<NoteName> = ArrayList()
            for (i in 0 until maxNumberOfNotes!!) {
                noteArrayList?.get(i)?.let { noteArrayList2.add(it) }
            }
            noteArrayList = noteArrayList2
        }
        this.countNotesDuration()
        val elements = IntArray(notesDuration.size)
        for (i in elements.indices) {
            elements[i] = notesDuration[i].numberOfNotes
        }
        var maxValue = elements.maxOrNull()
        val normalizedVector = DoubleArray(numberOfNotes)
        if (maxValue != null) {
            if (maxValue > 0) {
                for (i in 0 until numberOfNotes) {
                    normalizedVector[i] =
                        notesDuration[i].numberOfNotes.toDouble() / maxValue.toDouble()
                }
            }
        }
        return normalizedVector
    }

    private fun countNotesDuration() {
        for (note in noteArrayList!!) {
            for (scaleNote in notesDuration) {
                if (scaleNote.noteName === note) {
                    scaleNote.numberOfNotes += 1
                }
            }
        }
    }
}